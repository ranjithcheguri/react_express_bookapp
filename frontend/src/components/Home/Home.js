import React, {Component} from 'react';
import '../../App.css';
import axios from 'axios';
import cookie from 'react-cookies';
import {Redirect} from 'react-router';

class Home extends Component {
    constructor(){
        super();
        this.state = {  
            students : []
        }
    }  

    handleDelete=(deleteID)=>{
        console.log(deleteID);
        const data={
            deleteID : deleteID
        }
        axios.post('http://localhost:3001/delete',data)
       .then(response=>{
        this.setState({
            students : response.data 
        });      
     })
    }

    //get the students data from backend  
    componentDidMount(){
        axios.get('http://localhost:3001/home')
                .then((response) => {
                //update the state with the response data
                console.log(response.data);
                this.setState({
                    students : this.state.students.concat(response.data) 
                });
                console.log("students",this.state.students);
            });
    }

    render(){
        //iterate over students to create a table row
        let details = this.state.students.map(student => {
            return(
                <tr>
                    <td>{student.studentName}</td>
                    <td>{student.studentID}</td>
                    <td>{student.studentDept}</td>
                    <td><button onClick={this.handleDelete.bind(this,student.studentID)} className="btn btn-danger">  DELETE  </button></td>
                </tr>
            )
        })
        //if not logged in go to login page
        let redirectVar = null;
        if(!cookie.load('cookie')){
            redirectVar = <Redirect to= "/login"/>
        }
        return(
            <div>
                {redirectVar}
                <div class="container">
                    <h2>List of All students</h2>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>STUDENT NAME</th>
                                    <th>STUDENT ID</th>
                                    <th>STUDENT DEPARTMENT</th>
                                    <th>  DELETE  </th>
                                </tr>
                            </thead>
                            <tbody>
                                {/*Display the Tbale row based on data recieved*/}
                                {details}
                            </tbody>
                        </table>
                </div> 
            </div> 
        )
    }
}
//export Home Component
export default Home;