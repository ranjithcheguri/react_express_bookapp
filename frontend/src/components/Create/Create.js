import React, {Component} from 'react';
import axios from 'axios';
import {Redirect} from 'react-router';

class Create extends Component{
    constructor(props){
        super(props);

        this.studentIdChangeHandler = this.studentIdChangeHandler.bind(this);
        this.studentNameChangeHandler = this.studentNameChangeHandler.bind(this);
        this.studentDeptChangeHandler = this.studentDeptChangeHandler.bind(this);
        this.createNewStudents = this.createNewStudents.bind(this);

        this.state={
            studentID :"",
            studentName:"",
            studentDept:"",
            redirect : false
        }
    }

    studentIdChangeHandler = (e)=>{
        this.setState({
            studentID  :   e.target.value
        })
    }

    studentNameChangeHandler=(e)=>{
        this.setState({
            studentName   :   e.target.value
        })
    }

    studentDeptChangeHandler=(e)=>{
        this.setState({
            studentDept  :   e.target.value
        })
    }

    createNewStudents = (e)=>{
        const data = {
            studentID      :    this.state.studentID,
            studentName       :   this.state.studentName,
            studentDept  :    this.state.studentDept
        }
        axios.defaults.withCredentials = true;
        console.log(data);
        axios.post("http://localhost:3001/create",data)
        .then( response =>{ 
            console.log("res")
            this.setState({
                redirect : true
            })
        });
        e.preventDefault();
    }

    render(){
       var redirectVar = null;
        if(this.state.redirect){
            console.log("inside");
            redirectVar = <Redirect to="/home"/>;
        }
        return(
            <div>
                <br/>
                <div class="container">
                    {/* <form action="http://127.0.0.1:3000/create" method="post"> */}
                    {redirectVar}
                    <form>
                    <div style={{width: '30%'}} className="form-group">
                                <input  type="text"  value={this.state.studentName} onChange={this.studentNameChangeHandler} class="form-control" name="studentName" placeholder="STUDENT NAME"/>
                        </div>
                        <br/>
                       
                        <div style={{width: '30%'}} className="form-group">
                            <input  type="text" value={this.state.studentID} onChange={this.studentIdChangeHandler} class="form-control" name="studentID" placeholder="STUDENT ID"/>
                        </div>
                        <br/>
                        <div style={{width: '30%'}} className="form-group">
                                <input  type="text"  value={this.state.studentDept} onChange={this.studentDeptChangeHandler} class="form-control" name="studentDept" placeholder="STUDENT DEPARTMENT"/>
                        </div>
                        <br/>
                        <div style={{width: '30%'}}>
                            <button className="btn btn-success" onClick= {this.createNewStudents} type="submit">Create</button>
                        </div> 
                    </form>
                </div>
            </div>
        )
    }
}

export default Create;